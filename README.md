![Maze Game Deluxe](src/Maze%20Game%20Deluxe.jpg)
This is a JavaScript multi level Maze adventure game.
You can play it <a href="https://nateweiler.github.io/Maze-Game-Deluxe/" target="_blank">Here</a>. ***You must play with a keyboard⌨***

---

 This was created with:
* ***HTML5***
* ***CSS3***
* ***JavaScript***
---

## ***Usage***

This is for personal use ***ONLY*** as a simple game. You can download
the repo with

```sh
git clone https://github.com/NateWeiler/Maze-Game-Deluxe.git
```

---

## ***Contribution***

You can feel free to discuss anything that you feel could
be added or updated in the issues section.

Pull requests are more than welcome!

---
